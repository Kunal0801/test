from flask_pymongo import pymongo
import urllib.parse
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

class MongoDB:
    def __init__(self):
        self.mongodb_username = os.getenv("MONGODB_USERNAME")
        self.mongodb_password = os.getenv("MONGODB_PASSWORD")
        self.cluster_url = os.getenv("CLUSTER_URL")

        if not all([self.mongodb_username, self.mongodb_password, self.cluster_url]):
            raise ValueError("MongoDB environment variables are not set correctly")

        print(f"MONGODB_USERNAME: {self.mongodb_username}")
        print(f"MONGODB_PASSWORD: {self.mongodb_password}")
        print(f"CLUSTER_URL: {self.cluster_url}")

    def mongodb_connection(self):
        """
        This method establishes a connection to a MongoDB database

        :return: MongoClient instance
        """
        connection_string = "mongodb+srv://{}:{}@{}".format(
            urllib.parse.quote(self.mongodb_username),
            urllib.parse.quote(self.mongodb_password),
            self.cluster_url  # No need to quote the cluster URL
        )
        client = pymongo.MongoClient(connection_string)
        return client

    def save_data_mongodb(self, data):
        mongodb_client = self.mongodb_connection()
        mongodb_client.cbd3345_s24_s1.userinfo.insert_one(data)

# Example usage
if __name__ == "__main__":
    db_instance = MongoDB()
    sample_data = {"name": "John Doe", "email": "john.doe@example.com"}
    db_instance.save_data_mongodb(sample_data)
