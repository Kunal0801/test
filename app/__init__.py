import os

from flask import Flask
from dotenv import load_dotenv

import dns.resolver
dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
dns.resolver.default_resolver.nameservers = ['8.8.8.8']

app = Flask(__name__)
load_dotenv(str(os.getcwd()) + "/.env")
# Syntax: os.getenv(<ENVIRONMENT_VARIABLE_NAME>)

from app import routes


